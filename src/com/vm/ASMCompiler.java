package com.vm;

import com.vm.commands.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ASMCompiler {

    private static ASMCompiler mInstance;
    public static ASMCompiler getInstance() {
        if (mInstance == null) {
            mInstance = new ASMCompiler();
        }
        return mInstance;
    }

    private Scanner mScanner;
    private List<Command> mCommands = new ArrayList<Command>();
    private int mIp = 0;
    private String mAsmFile;
    private String mSrcFile;

    public void init(String dest) {
        mAsmFile = dest;
        mCommands.add(new Print());
        mCommands.add(new Word());
        mCommands.add(new Halt());
        mCommands.add(new Move());
        mCommands.add(new Add());
        mCommands.add(new Jump());
        mCommands.add(new Compare());
        mCommands.add(new Ja());
        mCommands.add(new Jb());
        mCommands.add(new Je());
        mCommands.add(new Push());
        mCommands.add(new Pop());
        mCommands.add(new Call());
        mCommands.add(new Ret());
        mCommands.add(new Get());
    }

    public void loadText(String path) {
        mSrcFile = path;
    }


    public void loadJumps() {
        try {
            Scanner tmp = new Scanner(new File(mSrcFile));
            mIp = 3;
            while (tmp.hasNextLine()) {
                String str = tmp.nextLine();
                if (str.trim().equals("")) {
                    continue;
                }
                if (!str.contains(":")) {
                    mIp++;
                } else {
                    str = str.substring(0, str.indexOf(':'));
                    Context.getInstance().set(str, mIp);
                }
            }
            tmp.close();

            mScanner = new Scanner(new File(mSrcFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void compile() {
        loadJumps();
        int line = 1;
        try {

            mIp = 3;

            while (mScanner.hasNext()) {
                String str = mScanner.nextLine();
                boolean flag = false;
                if (str.trim().equals("")) {
                    continue;
                }
                if (!str.contains(":")) {
                    for (Command cmd : mCommands) {
                        if (cmd.isInstance(str)) {
                            cmd.saveBytes();
                            flag = true;
                            break;
                        }
                    }
                    if (flag == false) {
                        throw new CompilerException("Invalid command " + str);
                    }
                } else {
                    //str = str.substring(0, str.indexOf(':'));
                    //Context.getInstance().set(str, mIp);
                }
                line++;
            }

            Integer start = Context.getInstance().get("start");
            if (start == null) {
                throw new CompilerException("There is no 'start:' in program!");
            }

            // instruction pointer
            VirtualMachine.getIsntance().setBytes(0, Utils.getBytesFromInt(start));
            // stack pointer for future
            //VirtualMachine.getIsntance().setBytes(1, Utils.getBytesFromInt(0));
            // next;

        } catch (CompilerException e) {
            System.err.println("Line " + Integer.toString(line) + " " + e.getMessage());
        }
        VirtualMachine.getIsntance().saveBin(mAsmFile);
    }



    public void addIp() {
        mIp++;
    }

    public int getIp() {
        return mIp;
    }
}
