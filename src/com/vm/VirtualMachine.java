package com.vm;

import com.vm.commands.Utils;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class VirtualMachine {
    private static VirtualMachine instance;
    byte[] mMemory;
    public String mPath;


    public int getSize() { return 1024; }
    private VirtualMachine() {
        setMemorySize(1024);
    }

    public static VirtualMachine getIsntance() {
        if (instance == null) {
            instance = new VirtualMachine();
        }
        return instance;
    }

    public byte[] getBytes(int index) {
        int i = index * 4;
        byte[] res = new byte[4];
        res[0] = mMemory[i];
        res[1] = mMemory[i + 1];
        res[2] = mMemory[i + 2];
        res[3] = mMemory[i + 3];
        return res;
    }

    public void setBytes(int index, byte[] bytes) {
        int i = index * 4;
        mMemory[i] = bytes[0];
        mMemory[i + 1] = bytes[1];
        mMemory[i + 2] = bytes[2];
        mMemory[i + 3] = bytes[3];
    }

    public void setMemorySize(int size) {
        mMemory = new byte[size * 4];
        setBytes(1,Utils.getBytesFromInt(size - 1));
    }

    public void saveBin(String path) {
        try {
            Files.write(FileSystems.getDefault().getPath(path), mMemory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadBin(String path) {
        mPath = path;
        try {
            mMemory = Files.readAllBytes(FileSystems.getDefault().getPath(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
