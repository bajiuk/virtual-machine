package com.vm;

import com.vm.commands.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MachineCodeInterpreter {
    private MachineCodeInterpreter() {
        init();
    }

    private static MachineCodeInterpreter mInstance;
    public static MachineCodeInterpreter getInstance() {
        if (mInstance == null) {
            mInstance = new MachineCodeInterpreter();
        }
        return mInstance;
    }

    private List<Command> mCommands = new ArrayList<Command>();

    public void init() {
        mCommands.add(new Print());
        mCommands.add(new Word());
        mCommands.add(new Halt());
        mCommands.add(new Move());
        mCommands.add(new Add());
        mCommands.add(new Jump());
        mCommands.add(new Compare());
        mCommands.add(new Ja());
        mCommands.add(new Jb());
        mCommands.add(new Je());
        mCommands.add(new Push());
        mCommands.add(new Pop());
        mCommands.add(new Call());
        mCommands.add(new Ret());
        mCommands.add(new Get());
    }


    public void execute() {
        System.out.println("Running " + VirtualMachine.getIsntance().mPath);
        try {
            while (true) {
                int mIp = Utils.getIntFromBytes(VirtualMachine.getIsntance().getBytes(0));
                byte[] str = VirtualMachine.getIsntance().getBytes(mIp);
                for (Command cmd : mCommands) {
                    if (cmd.isInstance(str)) {
                        cmd.execute();
                        break;
                    }
                }
            }
        } catch (VirtualMachineExitException e) {


        }
    }
}
