package com.vm;

public class Main {

    public static void main(String[] args) {
	    //ASMCompiler compiler = ASMCompiler.getInstance();
        //compiler.loadText("D:\\abbyy\\program.txt");
        //compiler.init("D:\\abbyy\\program.asm");
        //compiler.compile();
        //MachineCodeInterpreter interpreter = MachineCodeInterpreter.getInstance();
        //VirtualMachine.getIsntance().loadBin("D:\\abbyy\\program.asm");
        //interpreter.execute();
        if (args.length > 1) {
            String cmd = args[0];
            if (cmd.equals("dis")) {
                Disassembler d = Disassembler.getInstance();
                d.loadAsm(args[1]);
                d.init();
                d.saveAsm();
                d.execute();
                System.out.println("File " + args[1].substring(0, args[1].lastIndexOf('.')) + ".dis" + " created!");
            }

            if (cmd.equals("compile")) {
                ASMCompiler compiler = ASMCompiler.getInstance();
                compiler.loadText(args[1]);
                String acm = args[1].substring(0, args[1].lastIndexOf('.')) + ".asm";
                compiler.init(acm);
                compiler.compile();
                System.out.println("File " + args[1].substring(0, args[1].lastIndexOf('.')) + ".asm" + " created!");
            }

            if (cmd.equals("run")) {
                MachineCodeInterpreter interpreter = MachineCodeInterpreter.getInstance();
                VirtualMachine.getIsntance().loadBin(args[1]);
                interpreter.execute();
            }
        } else {
            System.out.println("Usage:");
            System.out.println("dis <file>");
            System.out.println("compile <file>");
            System.out.println("run <file>");
        }

    }
}
