package com.vm.commands;

import com.vm.VirtualMachine;

public class Move extends Command {

    public Move() {
        super((byte) 3, "MOV");
    }

    private int mSrc = 0;
    private int mDest = 0;

    @Override
    public boolean  isInstance(String name) throws CompilerException {
        if (super.isInstance(name) == false) {
            return false;
        }
        if (mArguments[1].charAt(0) >= '0' &&  mArguments[1].charAt(0) <= '9') {
            mSrc = Integer.parseInt(mArguments[1]);
        } else {
            Integer tmp = Context.getInstance().get(mArguments[1]);
            if (tmp == null) {
                throw new CompilerException("Invalid identifier: " + mArguments[1]);
            } else {
                mSrc = tmp;
            }
        }
        if (mArguments[2].charAt(0) >= '0' &&  mArguments[2].charAt(0) <= '9') {
            mDest = Integer.parseInt(mArguments[2]);
        } else {
            Integer tmp = Context.getInstance().get(mArguments[2]);
            if (tmp == null) {
                throw new CompilerException("Invalid identifier: " + mArguments[2]);
            } else {
                mDest = tmp;
            }
        }
        return true;
    }

    @Override
    public void saveBytes() {
        byte[] tmp = Utils.getBytesFromInt(mSrc);
        mCommand[1] = tmp[3];
        tmp = Utils.getBytesFromInt(mDest);
        mCommand[2] = tmp[2];
        mCommand[3] = tmp[3];
        super.saveBytes();
    }


    @Override
    public int[] getDataCells() {
        int[] ans = new int[2];
        ans[0] = (int)(mCommand[1] & 0xFF);
        ans[1] = ((int)(mCommand[2] & 0xFF) << 8) + (int) (mCommand[3] & 0xFF);
        return ans;
    }

    public void execute() {
        int index1 = (int)(mCommand[1] & 0xFF);
        int index2 = ((int)(mCommand[2] & 0xFF) << 8) + (int) (mCommand[3] & 0xFF);
        byte[] data = VirtualMachine.getIsntance().getBytes(index2);
        VirtualMachine.getIsntance().setBytes(index1, data);
        incIp();
    }
}
