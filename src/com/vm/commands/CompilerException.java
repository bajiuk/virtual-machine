package com.vm.commands;

public class CompilerException extends Exception {
    public CompilerException(){}
    public CompilerException(String message) {
        super(message);
    }
}
