package com.vm.commands;

import com.vm.VirtualMachine;

public class Pop extends Command {

    public Pop() {
        super((byte) 11, "POP");
    }

    private int mSrc = 0;

    @Override
    public boolean  isInstance(String name) throws CompilerException {
        if (super.isInstance(name) == false) {
            return false;
        }
        if (mArguments[1].charAt(0) >= '0' &&  mArguments[1].charAt(0) <= '9') {
            mSrc = Integer.parseInt(mArguments[1]);
        } else {
            Integer tmp = Context.getInstance().get(mArguments[1]);
            if (tmp == null) {
                throw new CompilerException("Invalid identifier: " + mArguments[1]);
            } else {
                mSrc = tmp;
            }
        }
        return true;
    }

    @Override
    public void saveBytes() {
        byte[] tmp = Utils.getBytesFromInt(mSrc);
        mCommand[3] = tmp[3];
        mCommand[2] = tmp[2];
        mCommand[1] = tmp[1];
        super.saveBytes();
    }

    @Override
    public int[] getDataCells() {
        int[] ans = new int[1];
        ans[0] = Utils.getIntFromByte3(mCommand);
        return ans;
    }

    public void execute() {
        int index = Utils.getIntFromByte3(mCommand);
        VirtualMachine.getIsntance().setBytes(index, pop());
        incIp();
    }
}
