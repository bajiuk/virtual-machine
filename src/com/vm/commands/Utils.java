package com.vm.commands;

public class Utils {
    public static int getIntFromBytes(byte[] src) {
        int a = 0;
        a = src[0] & 0xFF;
        a = (a << 8) + (int) (src[1] & 0xFF);
        a = (a << 8) + (int) (src[2] & 0xFF);
        a = (a << 8) + (int) (src[3] & 0xFF);
        return a;
    }

    public static int getIntFromByte3(byte[] src) {
        int a = 0;
        a = src[1];
        a = (a << 8) + src[2];
        a = (a << 8) + src[3];
        return a;
    }

    public static byte[] getBytesFromInt(int src) {
        byte[] a = new byte[4];
        a[0] = (byte) ((src >> 24) & 0xFF);
        a[1] = (byte) ((src >> 16) & 0xFF);
        a[2] = (byte) ((src >> 8) & 0xFF);
        a[3] = (byte) ((src >> 0) & 0xFF);
        return a;
    }
}
