package com.vm.commands;

import com.vm.ASMCompiler;
import com.vm.VirtualMachine;

import javax.rmi.CORBA.Util;

public abstract class Command {

    protected byte mId;
    protected String mName;
    protected byte[] mCommand;
    protected String[] mArguments;

    public Command(byte id, String name) {
        mCommand = new byte[4];
        mId = id;
        mName = name;
    }

    public byte getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }


    public boolean isInstance(String name) throws CompilerException {
        mArguments = name.split(" ");
        return mName.equals(mArguments[0]);
    }

    public boolean isInstance(byte[] bytes) {
        mCommand = bytes;
        return mCommand[0] == mId;
    }


    public void setBytes(byte[] bytes) {
        mCommand = bytes;
    }

    public int getWord(int word) {
        int ans = mCommand[3];
        ans = ans & mCommand[2] << 8;
        ans = ans & mCommand[1] << 16;
        ans = ans & mCommand[0] << 24;
        return ans;
    }

    public byte[] getBytes() {
        mCommand[0] = mId;
        return mCommand;
    }

    public void setByte(int index, byte value) {
        mCommand[index] = value;
    }

    public void saveBytes() {
        mCommand[0] = mId;
        VirtualMachine.getIsntance().setBytes(ASMCompiler.getInstance().getIp(), mCommand);
        ASMCompiler.getInstance().addIp();
    }

    public void incIp() {
        int ip = Utils.getIntFromBytes(VirtualMachine.getIsntance().getBytes(0));
        ip += 1;
        VirtualMachine.getIsntance().setBytes(0, Utils.getBytesFromInt(ip));
    }

    public void setIp(int ip) {
        VirtualMachine.getIsntance().setBytes(0, Utils.getBytesFromInt(ip));
    }

    public void pushIp() {
        int ip = Utils.getIntFromBytes(VirtualMachine.getIsntance().getBytes(0));
        ip++;
        push(Utils.getBytesFromInt(ip));
    }

    public void popIp() {
        byte[] ip = pop();
        VirtualMachine.getIsntance().setBytes(0,ip);
    }

    public void push(byte[] word) {
        int sp = Utils.getIntFromBytes(VirtualMachine.getIsntance().getBytes(1));
        VirtualMachine.getIsntance().setBytes(sp, word);
        sp -= 1;
        VirtualMachine.getIsntance().setBytes(1, Utils.getBytesFromInt(sp));
    }

    public byte[] pop() {
        int sp = Utils.getIntFromBytes(VirtualMachine.getIsntance().getBytes(1));
        sp += 1;
        byte[] res = VirtualMachine.getIsntance().getBytes(sp);
        VirtualMachine.getIsntance().setBytes(1, Utils.getBytesFromInt(sp));
        return res;
    }


    public int[] getDataCells() {
        return null;
    }
    public int[] getIndexCells() {
        return null;
    }

    abstract public void execute();
}
