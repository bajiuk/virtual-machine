package com.vm.commands;

import java.util.HashMap;
import java.util.Map;

public class Context {

    private static Context mContext;

    public static Context getInstance() {
        if (mContext != null) {
            return mContext;
        }
        mContext = new Context();
        return mContext;
    }

    private Map<String, Integer> lParam = new HashMap<String, Integer>();

    public Integer get(String name) {
        return lParam.get(name);
    }
    public void set(String name, int value) { lParam.put(name, value); }

}
