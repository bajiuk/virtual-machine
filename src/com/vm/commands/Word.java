package com.vm.commands;

import com.vm.ASMCompiler;
import com.vm.VirtualMachine;

public class Word extends Command {

    public Word() {
        super((byte) 0, "WORD");
    }

    private int mSrc = 0;
    private String mName = "";

    @Override
    public boolean  isInstance(String name) throws CompilerException {
        if (super.isInstance(name) == false) {
            return false;
        }
        if (!(mArguments[1].charAt(0) >= '0' &&  mArguments[1].charAt(0) <= '9')) {
            mSrc = Integer.parseInt(mArguments[2]);
            mName = mArguments[1];
        } else {
            throw new CompilerException("Invalid identifier: " + mArguments[1]);
        }
        return true;
    }

    @Override
    public void saveBytes() {
        byte[] tmp = Utils.getBytesFromInt(mSrc);
        mCommand[3] = tmp[3];
        mCommand[2] = tmp[2];
        mCommand[1] = tmp[1];
        mCommand[0] = tmp[0];
        Context.getInstance().set(mName, ASMCompiler.getInstance().getIp());
        super.saveBytes();
        mCommand[0] = tmp[0];
    }

    @Override
    public int[] getIndexCells() {
        int[] ans = new int[1];
        ans[0] = Utils.getIntFromByte3(mCommand);
        return ans;
    }

    public void execute() {
       // no
    }
}