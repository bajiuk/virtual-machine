package com.vm.commands;

public class Ret extends Command {

    public Ret() {
        super((byte) 13, "RET");
    }

    @Override
    public boolean  isInstance(String name) throws CompilerException {
        if (super.isInstance(name) == false) {
            return false;
        }
        return true;
    }

    public void execute() {
        popIp();
    }
}
