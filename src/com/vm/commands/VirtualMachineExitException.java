package com.vm.commands;

public class VirtualMachineExitException extends Error {
    public VirtualMachineExitException(){}
    public VirtualMachineExitException(String message) {
        super(message);
    }
}
