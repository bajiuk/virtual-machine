package com.vm.commands;

public class Halt extends Command {
    public Halt() {
        super((byte) 2, "HALT");
    }

    private int mExitCode = 0;

    @Override
    public boolean  isInstance(String name) throws CompilerException {
        if (super.isInstance(name) == false) {
            return false;
        }
        if (mArguments[1].charAt(0) >= '0' &&  mArguments[1].charAt(0) <= '9') {
            mExitCode = Integer.parseInt(mArguments[1]);
        } else {
            throw new CompilerException("Invalid identifier: " + mArguments[1]);
        }
        return true;
    }

    @Override
    public void saveBytes() {
        byte[] tmp = Utils.getBytesFromInt(mExitCode);
        mCommand[3] = tmp[3];
        mCommand[2] = tmp[2];
        mCommand[1] = tmp[1];
        super.saveBytes();
    }

    @Override
    public int[] getIndexCells() {
        int[] ans = new int[1];
        ans[0] = Utils.getIntFromByte3(mCommand);
        return ans;
    }

    public void execute() {
        int res = Utils.getIntFromByte3(mCommand);
        System.out.println("Exited with code: " + Integer.toString(res));
        throw new VirtualMachineExitException("Exit with code: " + Integer.toString(res));
    }

}
