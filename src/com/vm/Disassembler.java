package com.vm;

import com.vm.commands.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Disassembler {
    private static Disassembler mInstance;
    public static Disassembler getInstance() {
        if (mInstance == null) {
            mInstance = new Disassembler();
        }
        return mInstance;
    }

    private String mName;
    private PrintWriter mOutput;


    public void loadAsm(String path) {
        mName = path;
        VirtualMachine.getIsntance().loadBin(path);
    }

    public void saveAsm() {
        String name = mName.substring(0,mName.lastIndexOf('.'));
        name += ".dis";
        File flt = new File(name);
        try {
            mOutput = new PrintWriter(new BufferedWriter(new FileWriter(flt)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Command> mCommands = new ArrayList<Command>();

    public void init() {
        mCommands.add(new Print());
        mCommands.add(new Halt());
        mCommands.add(new Move());
        mCommands.add(new Add());
        mCommands.add(new Jump());
        mCommands.add(new Compare());
        mCommands.add(new Ja());
        mCommands.add(new Jb());
        mCommands.add(new Je());
        mCommands.add(new Push());
        mCommands.add(new Pop());
        mCommands.add(new Call());
        mCommands.add(new Ret());
        mCommands.add(new Get());
    }

    private Set<Integer> mData = new HashSet<Integer>();


    private void printFormatted(int index, String command) {
        String a = Integer.toString(index);
        while (a.length() < 5) {
            a = "0" + a;
        }
        mOutput.println(a + "|" + command);
    }


    public void execute() {
        System.out.println("Disassembling " + mName);
        int size = VirtualMachine.getIsntance().getSize();
        int currentIndex = 0;
        while (currentIndex < size) {
            byte[] cmd = VirtualMachine.getIsntance().getBytes(currentIndex);
            boolean isMemory = true;
            for(Command c : mCommands) {
                if (c.isInstance(cmd)) {
                    int[] data = c.getDataCells();
                    isMemory = false;
                    if (data == null) {
                        break;
                    }
                    for (int i = 0; i < data.length; ++i) {
                        mData.add(data[i]);
                    }
                    break;
                }
            }
            if (isMemory) {
                mData.add(currentIndex);
            }
            currentIndex++;
        }

        currentIndex = 0;
        while (currentIndex < size) {
            byte[] cmd = VirtualMachine.getIsntance().getBytes(currentIndex);
            if (mData.contains(currentIndex)){
                String data = "WORD _" + Integer.toString(currentIndex) + " " + Utils.getIntFromBytes(cmd);
                printFormatted(currentIndex,data);
            } else {
                for (Command c : mCommands) {
                    if (c.isInstance(cmd)) {
                        String var = c.getName() + " ";
                        if (c.getDataCells() != null) {
                            int[] data = c.getDataCells();
                            for (int i = 0; i < data.length; ++i) {
                               var = var + "_" + Integer.toString(data[i]);
                            }
                        }
                        if (c.getIndexCells() != null) {
                            int[] data = c.getIndexCells();
                            for (int i = 0; i < data.length; ++i) {
                                var = var + Integer.toString(data[i]);
                            }
                        }
                        printFormatted(currentIndex, var);
                        break;
                    }
                }
            }
            currentIndex++;
        }
        mOutput.flush();
        mOutput.close();
    }
}
